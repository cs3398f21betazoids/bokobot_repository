## Boko Bot

![](https://i.imgur.com/z1fncs8.jpg)

## Table of Contents
* [Overview](#markdown-header-overview)
* [Features](#markdown-header-features)
* [Technologies Used](#markdown-header-technologies-used)
* [Dependencies](#markdown-header-dependencies)
* [Setup](#markdown-header-setup)
* [Usage](#markdown-header-usage)
* [Project Status](#markdown-header-project-status)
* [Screenshots](#markdown-header-screenshots)
* [Room For Improvement](#markdown-header-room-for-improvement)

## Overview

**Version: 1.0.00**

**What is Boko Bot?**  
A multifunctional Discord bot for Texas State Students. It provides students with quick access to Texas State events going on that day. It also gives students direct links to review or leave comments on RateMyProfessor profiles. Last, it's an easy way to stay connected and wish your TX State friends "Happy Birthday!"


## Features

### How does it work?

#### Birthday Feature:
* The bot will send each member who has entered their birthdate a "Happy Birthday!" message on their Birthday. 
* The bot will respond to `<command>` with a list of members that have a Birthday that day. 
* The bot will allow the user to send a "Happy Birthday!" message to members selected by the user. 

`User Story:  I, Miussett Salomon, as a student of Texas State University would like to be able to know and tell my friends Happy Birthday.`

#### Bus Route Feature:
* When a user enters command `<current stop>` `<destination stop>`, the bot tells the user what bus route to take and a link to the route schedule on texas state website.
* The bot will give the number of stops in between.
* (pending) The bus will let you know what bus stops are near the nearest buildings

`User Story:  I, Jacob Christian, as a TXST student, I would like to put my current stop and my destination stop and have a bot tell me which route to take.`

#### Texas State Events Feature:
* When a user enters command -event the bot posts a list of events happening at the university that day. 
* The bot will also store event information as inputted by club administrator using `<command>`. 

`User Story -> I, Yrua, as a student of Texas State University, would like to be able to find events in my school.`

## Technologies Used
* Discord
* Java
* Gradle 

## Dependencies

### Gradle

    dependencies {
        implementation 'junit:junit:4.13.1'
    
        // Use JUnit test framework.
        //testImplementation 'junit:junit:4.13.2'
        testImplementation 'org.junit.jupiter:junit-jupiter-api:5.3.1'
        testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.3.1'
    
        // This dependency is used by the application.
        implementation 'com.google.guava:guava:30.1-jre'
        implementation 'com.sedmelluq:lavaplayer:1.3.77'
    
        //BokoBot
        implementation("net.dv8tion:JDA:4.3.0_300")
        implementation 'org.slf4j:slf4j-simple:1.7.31'
        implementation("net.dv8tion:JDA:4.3.0_277")
        implementation 'org.xerial:sqlite-jdbc:3.36.0.2'
    
        //Google Maps
        implementation 'com.google.maps:google-maps-services:1.0.0'
    
        //JSON Parsing library
        implementation 'com.google.code.gson:gson:2.8.9'
    
        //JavaTuple
        implementation 'org.javatuples:javatuples:1.2' 
    }  
    

## SetUp
1) Clone the BokoBot repository : `git clone https://jbc160@bitbucket.org/cs3398f21betazoids/bokobot_repository.git`  
2) Visit The Discord Developer Portal : https://discord.com/developers/applications  
3) Create your bot, and generate a token.  
4) Pass the token into `JDABuilder.createDefault(System.getenv("BOT_TOKEN")` in `App.java`


### Notes
1) The bot **will not run** if you do not have a proper Discord Bot token.   
2) Make sure you run the bot with the token in environment variables. You may pass in the direct token if you wish, but that is not secure. Just include `"BOT_TOKEN=<token>"` in the system enviornment


## Usage

**General Commands**
```
-waddup   : Returns "wadddduuuup" to the user
-hmmm     : Returns "*cloaked man removes hood* Hello there."
-hippity  : Returns "Hop"
-bday     : Returns "Happy Birthday!"
-neighbor : Returns "Often when you think you're at the end of something, you're at the beginning of something else."
-sqlTest <name> <age> <address> <salary> : Adds arguments into SQLite3 database and returns all current database records.
-addBirthday <@userID> <birthday> : Adds arguments into SQLite3 database
-birthdaysList : Returns all current database records
-birthdaysToday : Returns all current database records for birthdays that day
-sendBirthdayMessage <message>: Sends custom message to all users with birthdays that day 
-deleteBirthday <@userID> : Deletes record that pertains to id 
-closestStop <address> : Gives the closest bus stop and current bus route to campus
-event%<Event Name>%<Time>%<Date>%<Location>%<Description>: Adds event to database and displays it
-list: Lists all the events saved
-remove%<Event Name>: Removes event named from database
-display%<Event Name>: Displays the information of the event named
-TXSTEvent: Posts the link to the Texas State Events site
```
___
## Sprint 1
*Note: Research implementations and findings located in Research file within this directory*
Research file direct link: https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/897df7e0c3aa/Researc?at=master

* Each team member implemented their own bot using JDA and added it to team's Discord server.
* Each team member followed a tutorial on creating an SQLlite3 database with JDBC
* Each team member followed a tutorial on CRUD operations on JDBC

Added custom bots to Discord server

![](https://i.imgur.com/GGBuYg1.png)
___
## Sprint 2
*Note: Research implementations and findings located in Researc file within this directory*
Research file direct link: https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/897df7e0c3aa/Researc?at=master
### Jacob Christian:   

* URL Reference:  
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/BusRouteController.java  
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/BusRoute.java  
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/BusStop.java  

![](https://i.imgur.com/XX6NKTg.png)
#### Status:  

* Manually Gathered GPS coordinates of all Tx State Bus Stops. Artifact is in root directory as "Bus Route GPS Information"  
* Added bot commands to get GPS coordinates from a bus stop  
* Added a -commands command to list all currently available commands from the bot  

#### Next Steps:   

* Work with the data retrieved and Stephen's Matrix API implementation to get bot fully realized.  
* Add unit testing for the bus driver controller.

### Nicolas Gonzales:

![](https://i.imgur.com/PLwn4P5.png)
#### Status:

* URL Reference: 
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/EventController.java
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/JdbcController.java

* Created Database to hold event information
* Added bot commands to retrieve event information from user
* Added code to parse input arguments and add to the database

#### Next Steps:

* Implement functionality to retrieve event information based on date or name of event
* Add functionality to allow users to subscribe to the event


### Miussett Salomon:

* URL Reference: https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/EventController.java

![](https://i.imgur.com/uTX1ODF.png)
![](https://i.imgur.com/BqzkmnI.png)
##### Status:

* Created EventController class
* Implemented event display template
* Created event creation commands
* Created and implemented command to retrieve link to Texas State Events Page

##### Next Steps:

* Implement functionality to retrieve event information based on date or name of event
* Add functionality to allow users to subscribe to the event

### Yrua Riley:
* URL: 
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/BirthdayController.java
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/Birthday.java
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/DateFormatter.java

![](https://i.imgur.com/9Md0SEA.png)
![](https://i.imgur.com/XwEtZ0x.png)
#### Status:

* Created Birthday class and database to hold birthday information
* Added functionality, such as adding, deleting and querying a birthday. 
#### Next Steps: 

* Add functionality for user to send Happy Birthday message to another user
* Research sql injection and update sql queries. 
* Clean up birthday code.  
* Extract birthday controls from main botcontroller.java class.
* Concentrate on refactoring code. 
 
 
###Stephen Burling
Sprint 2 Tasks: BOKO-74-86

* URL:
    * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/
    * Description: 74 - Obtained trial for Google Matrix API and activated our API key for use in RESTful API between our bot and Google Maps.
            Basic format for REST API call will be https://maps.googleapis.com/maps/api/distancematrix/outputFormat?parameters, and in Java the call will look like:  
  
                OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
                Request request = new Request.Builder()
                  .url("https://maps.googleapis.com/maps/api/distancematrix/json?origins=40.6655101%2C-73.89188969999998&destinations=40.659569%2C-73.933783%7C40.729029%2C-73.851524%7C40.6860072%2C-73.6334271%7C40.598566%2C-73.7527626&key=YOUR_API_KEY")
                  .method("GET", null)
                  .build();
       * Will be getting API info from https://developers.google.com/maps/documentation/distance-matrix/overview#maps_http_distancematrix_latlng-java
       * Will implement this code into bot by Friday.
    * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/BusRouteController.java
    * Description: 86 - Me and Jacob discussed final implementation changes and applied them in the actual command design:  
    ```Response response = client.newCall(request).execute();```


## Room for improvement
* Jacob Christian: (Sprint 2) Refactor data tables and how the csv file is parsed. Maybe look into threading for faster read time?
* Nicolas Gonzales: Reduce time spent on tasks and ask for help if needed
                  Refactor pushed code
* Yrua Riley : Research sql injection and apply safeguards to current sql queries.
               Get better at estimating time needed to complete tasks. I underestimated the time it would take to complete most tasks in this sprint.

___

## Sprint 3
*NOTE: Demo Branch is origin - main*

### Stephen Burling:
#### URLs/Artifacts:

* Google API Handler Class: https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/GoogleHandler.java
    * Description: Created entire class for handling Google API requests, particularly those calling Google Geocoder API (linked above).
* Bot Controller: https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/BotController.java
    * Description: Added a bot command (-coords) to help assess the success of Google Geocoder API usage, but am going to leave it in as part of the bot for novelty, as another feature (linked above, lines 67-73).
        
    * Description: Added method to get coordinates of a spoken word location, which is used in other parts of the code tha need those Google API calls. This connects the Google Handler class code to the rest of the commands. [getCoordinates()]
        
        * Class call: 
        
        ![](https://i.imgur.com/M3wmkp2.png)
        
    * Description: Also changed the information passing in the code, changed all of the Bus Route argument passes to have more efficient CLI parsing tools. Specifically, instead of passing a String[] with all of the arguments, I made it so that it passes the entire array minus the X elements that were already interpreted by the app. This is to make sure no one method in Bus Route is dealing with information it does not need to have access to, like a previous CLI argument.
        
        * Example: 
        
        ![](https://i.imgur.com/U9uBBER.png)

### Jacob Christian:
#### URLs/Artifacts:
![](https://i.imgur.com/T4nm5jm.jpg)
![](https://i.imgur.com/l8WsxTb.jpeg)


  * Unit Testing: https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/test/java/BotPackage/BusRouteControllerTest.java
    * Description: Implemented unit testing for the bus controller. The idea was to get this working with CircleCI, but CircleCI had some errors. Still, the program runs just fine.
  * Bus Route Times Wrapper: https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/BusRouteTimesWrapper.java  
    * This wrapper was made to make the .csv of bus times easier to implement to ensure that the rider gets real time bus route information.
  * Bus Route Controller: https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/BusRouteController.java
    * Added files to handle real time bus route information, implemented data structures to get filtered bus information to the user.  

##### Status
  * Jacob Christian: Final Sprint Completed, got MVP up and running for presentation on 11/30
  * Jacob Christian: CircleCI still not working, was not able to figure out issue of gradle build before end of sprint.

##### Next Steps
  * Jacob Christian: Further work into bus route implementation (such as an automatic to/from command that will allow the user to say they're going to campus, or coming from campus).
  * Jacob Christian: Switch from Google to a free API so project will not cost money to use, then add it to various Texas State sponsored Discords.
  * Jacob Christian: Add the bot to a remote server so that it can be running consistently.
 
### Yrua Riley:
* All commits are tied to a specific BOKO task in commit log
#### URLs/Artifacts:  
* https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/BirthdayController.java
  * Database that holds all Birthday information was updated to return more concise results.
* https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/Birthday.java
  * Birthday class was updated to hold method for sending birthday message. 
* https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/DateFormatter.java
  * DateFormatter class had method to get today's date 
* https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/DateCustom.java
  * DateCustom class was created as a refactoring attempt. The dateCustom responsibility is to use date formatter to create different dates that may be needed by any other classes. 
* https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/BirthdayQueries.java
  * BirthdayQueries class was created to hold different queried that may be used by other classes to get information from the birthday db

##### Status
  * Yrua Riley: Working on polishing Birthday feature. Private message functionality was added and bot now sends custom messages to user's via a private channel. 

##### Next Steps
  * Yrua Riley: Need to implement safety for database. In addition, would like to make Birthday to look the same as the other

### Nicolas Gonzales:

#### URL's/Artifacts:
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/EventController.java
    * Implement Add Event, Delete Event, & List All Events functionality for events.
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/EventController.java
    * Refactored EventController in order to make it more readable and organized for future developers.
  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/EventController.java
    * Implemented a Display Event function when choosing an event to look into.

##### Status
  * Nicolas Gonzales: Final Sprint Completed 

##### Next Steps
  * Nicolas Gonzales: Continue to build upon project for practice.
  
### Miussett Salomon:
#### URLs/Artifacts:

  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/BokoBot/app/src/main/java/BotPackage/EventController.java

Created the initial EventController class. Implemented command for event creation. User enters in event information and Event message is displayed to all members. Also, added command for TxState students to have direct link to most current school events.

  * https://bitbucket.org/cs3398f21betazoids/bokobot_repository/src/master/Research

Completed research and added information to this file. Unable to implement the feature to decline or mark an event as tentative. Research file goes into additional detail.

##### Status: 

* Final Sprint completed. Extensive research but unable to implement tweaks to Event feature.

##### Next Steps: 

* Link current event database to a separate google calendar. This will add easier access to decline, mark event as tentative, or change status of RSVP.
___
## Sprint 3 Retrospective
### What Went Well or maybe not?

#### TEAM:
  * What Went Well : 
    * We were able to get a minimal viable product out by sprint 3. 
    * Helped each other find solutions and fix bugs in each other's code.
    * We were able to implement more features once we were all split and specialized on our own part of the Bot. 
  * What Did Not Go Well : 
    * Got flagged for not being up to speed during sprint standup. Burnup trend line and actual work completed were not at ideal rate.
    * Testing implementation was not completed. We had issues getting CircleCi to build successfully.

#### INDIVIDUAL:
  * Yrua Riley : 
    * Completed implementation of birthday message. Unfortunately, ran out of time and did not implement sql injection safeguards.  
  * Stephen Burling : 
    * Did not have time to finish writing up the '-bus to' command, which would have had the opposite effect of the current command (-closestStop, which get coordinates to the closest bus stop to take you to campus). I was meant to split the command into '-bus to' and '-bus from', but ran out of time in the implementation.  
  * Jacob Christian : 
    * After getting data structures from Stephen, we were able to get the full implementation completed. This sprint felt like everything fell into place to get the bus route implemented well enough for presentation.
  * Nick Gonzales:  
    *  Collaboration between teammates when encountering an issue went very well. Personally, Jacob was a big help when I was having trouble with my code.
  * Miussett Salomon :  
    *  The Event feature has been integrated well with the rest of the groups code/features. 

### What Might Be Impeding Us from Performing Better?
* Balancing each other's schedules to meet and discuss the project, as well as synchronizing meeting time. With the school schedule it's difficult to ensure that we're all giving equal work time consistently, whereas in a career position this would be less of an issue.

### What Can We do to Improve?

#### TEAM
  * Perhaps have an assigned Program Manager to keep track of our work and keep us on time. 

#### INDIVIDUAL  

  * Stephen Burling :   
    * I need to manage my time better on this project in regard to my other projects that I have going on. More specifically, I think I needed to further break down my stories into smaller and smaller tasks, as the tasks I was handling were incredibly broad and it was hard to gain footing after not working on the app for a couple of days.
  * Jacob Christian :  
    * Manage the work time for the sprint more consistently to follow the burnup chart in a more ideal fashion. Often my contributions are done during the weekend when class isn't starting, but that leaves a lot of space between the ideal burn rate, but on the weekend the actual work completed shoots up with a very steep slope. This is obviously not ideal, so finding time to work on the project a little each day will show a much more consistent picture about what's happening.  
  * Yrua Riley :   
    * Improve time estimates for completing tasks. For the future, ensure to match time/points to the team's overall estimates to make tasks consistent.  
  * Nick Gonzales:  
    *  Breakdown overall tasks into more manageable pieces to be dispersed among the sprints. Some sprints felt more loaded than others.
  * Miussett Salomon :  
    * I need to breakdown tasks into even smaller steps to track my time. This will also help me better track my progress and help me locate trouble areas much quicker.

